import csv
import sklearn
from sklearn import cluster
from sklearn.cluster import KMeans
from sklearn import metrics
from scipy.spatial.distance import cdist
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import silhouette_samples, silhouette_score

def mean(l):
	sum = 0
	for x in l:
		sum += x
	return sum/len(l)


array = []
with open('cars.csv') as myFile:  
	reader = csv.reader(myFile)
	for row in reader:
		array.append(row)

cars_list = array[1:]

X1 = []
X2 = []

for row in cars_list:
	X1.append(int(row[5]))
	X2.append(int(row[6]))

x1 = np.array(X1)
x2 = np.array(X2)

# plt.plot()
# plt.title('Cars')
# plt.scatter(x1, x2)
# plt.show()

# create new plot and data
plt.plot()
X = np.array(list(zip(x1, x2))).reshape(len(x1), 2)
colors = ['b', 'g', 'r']
markers = ['o', 'v', 's']

# k means determine k
ssd = []
K = range(1,16)
for k in K:
    kmeanModel = KMeans(n_clusters=k).fit(X)
    kmeanModel.fit(X)
    # distortions.append(sum(np.min(cdist(X, kmeanModel.cluster_centers_, 'euclidean'), axis=1)) / X.shape[0])
    ssd.append(kmeanModel.inertia_)

for k in K:
	print(str(k) + '\t' + str(ssd[k-1]))

# plt.xlim([x1.min(), x1.max()])
# plt.ylim([x2.min(), x2.max()])
plt.plot(K, ssd, 'bx-')
plt.xlabel('k')
plt.ylabel('Sum of Square Distances')
plt.title('The Elbow Method showing the optimal k')
plt.show()

Optimal_NumberOf_Components=K[ssd.index(max(ssd))]
print("Optimal number of components is: " + str(Optimal_NumberOf_Components))

obs = X
silhouette_score_values=list()
 
NumberOfClusters=range(2, 16)
 
for i in NumberOfClusters:
    classifier=cluster.KMeans(i,init='k-means++', n_init=10, max_iter=300, tol=0.0001, verbose=0, random_state=None, copy_x=True)
    classifier.fit(obs)
    labels= classifier.predict(obs)
    sil_val = sklearn.metrics.silhouette_score(obs,labels ,metric='euclidean', sample_size=None, random_state=None)
    silhouette_score_values.append(sil_val)
    print(str(i) + '\t' + str(sil_val))
 
plt.plot(NumberOfClusters, silhouette_score_values)
plt.title("Silhouette score values vs Numbers of Clusters ")
plt.show()
 
Optimal_NumberOf_Components=NumberOfClusters[silhouette_score_values.index(max(silhouette_score_values))]
print("Optimal number of components is: " + str(Optimal_NumberOf_Components))