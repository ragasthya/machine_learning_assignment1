import numpy as np
import matplotlib.pyplot as plt
import csv
from sklearn.cluster import KMeans
import pandas
 
array = []
with open('Spiral.csv') as myFile:  
	reader = csv.reader(myFile)
	for row in reader:
		array.append(row)

co_ordinates = array[1:]

X = []
Y = []
N = 0
trainData = []

for row in co_ordinates:
	N += 1
	X.append(float(row[1]))
	Y.append(float(row[2]))
	trainData.append([float(row[1]), float(row[2])])

x = np.array(X)
y = np.array(Y)


colors = (0,0,0)
area = np.pi*1
 
# Plot
plt.scatter(x, y, s=area, c=colors, alpha=0.5)
plt.title('Scatter plot')
plt.xlabel('x')
plt.ylabel('y')
plt.show()

kmeans = KMeans(n_clusters=2, random_state=0).fit(trainData)

print(str(kmeans.labels_))

plt.scatter(X, Y, c = kmeans.labels_)
plt.xlabel('x')
plt.ylabel('y')
plt.grid(True)
plt.show()

import sklearn.neighbors

# Three nearest neighbors
kNNSpec = sklearn.neighbors.NearestNeighbors(n_neighbors = 8, algorithm = 'brute', metric = 'euclidean')
nbrs = kNNSpec.fit(trainData)
d3, i3 = nbrs.kneighbors(trainData)

distObject = sklearn.neighbors.DistanceMetric.get_metric('euclidean')

distances = distObject.pairwise(trainData)

x = trainData

from numpy import linalg as LA
xtx = np.matmul(x, np.transpose(x))
evals, evecs = LA.eigh(xtx)

print(str(evals))
# Series plot of the smallest ten eigenvalues to determine the number of clusters
plt.scatter(np.arange(0,9,1), evals[0:9,])
plt.xlabel('Sequence')
plt.ylabel('Eigenvalue')
plt.show()

maps = {}
for i in range(0, len(evals)):
	maps[evals[i]] = evecs[i]

for key in sorted(maps)[:2]:
	print(str(key))

Z = evecs[:,[0,1]]
print(Z[[0]].mean(), Z[[0]].std())
print(Z[[1]].mean(), Z[[1]].std())

plt.scatter(Z[[0]], Z[[1]])
plt.xlabel('Z[0]')
plt.ylabel('Z[1]')
plt.show()

kmeans_spectral = KMeans(n_clusters=2, random_state=0).fit(Z)

Spiral = pandas.read_csv('Spiral.csv',delimiter=',')

Spiral['KMeanCluster'] = kmeans.labels_

# for i in range(2):
#     print("Cluster Label = ", i)
#     print(Spiral.loc[Spiral['KMeanCluster'] == i])


plt.scatter(Spiral[['x']], Spiral[['y']], c = Spiral[['SpectralCluster']])
plt.xlabel('x')
plt.ylabel('y')
plt.grid(True)
plt.show()
