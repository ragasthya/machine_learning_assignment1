import csv
import math
import numpy as np
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt

array = []
items = {}
with open('Groceries.csv') as myFile:  
	reader = csv.reader(myFile)
	for row in reader:
		array.append(row)

groceries_list = array[1:]

for item in groceries_list:
	if item[1] in items.keys():
		items[item[1]] = items[item[1]] + 1
	else:
		items[item[1]] = 1

customers = {}
for item in groceries_list:
	if item[0] in customers.keys():
		array = customers[item[0]]
		if not item[1] in array:
			array.append(item[1])
			customers[item[0]] = array
	else:
		array = []
		array.append(item[1])
		customers[item[0]] = array

x = []
for key in customers.keys():
	x.append(len(customers[key]))

# print('Number of customers: ' + str(len(groceries_list)))

# print('Unique Items: ' + str(len(customers.keys())))

num_bins = math.ceil(math.sqrt(len(x)))
bin_width = float(max(x) - min(x))/num_bins

print(str(num_bins))
print(str(bin_width))

n, bins, patches = plt.hist(x, num_bins, facecolor='blue', alpha=bin_width)
plt.show()

median = np.percentile(x, 50)
quartile_1 = np.percentile(x, 25)
quartile_3 = np.percentile(x, 75)

print("Median: " + str(median))
print("1st Quartile: " + str(quartile_1))
print("3rd Quartile: " + str(quartile_3))

item_sets = {}

for customer in customers:
	if str(customers[customer]) in item_sets.keys():
		item_sets[str(customers[customer])] = item_sets[str(customers[customer])] + 1
	else:
		item_sets[str(customers[customer])] = 1

max_val = 0
for item in sorted(item_sets):
	val = item_sets[item]
	if (val > max_val):
		max_val = val

print(str(len(item_sets)))
print(str(max_val))
