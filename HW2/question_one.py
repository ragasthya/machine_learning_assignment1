def get_power_set(s):
    power_set = [set()]
    for element in s:
        new_sets = []
        for subset in power_set:
            new_sets.append(subset | {element})
        power_set.extend(new_sets)
    power_set.sort(key=len)
    return power_set

array = {'A', 'B', 'C', 'D', 'E', 'F', 'G'}
power = get_power_set(array)
len_init = 0
for set_elem in power[:-1]:
	if(not (len(set_elem) == len_init)):
		print('\n')
		len_init = len(set_elem)
	print('{'),
	for elem in set_elem[:-1]:
		print(elem + ',\t'),
	print(set_elem[-1]),
	print('},\t'),

set_elem = power[-1]
print('{'),
for elem in set_elem[:-1]:
	print(elem + ',\t'),
print(set_elem[-1]),
print('}\t'),


num = 2**(len(array)) - 1
print('Number of possible item sets: ' + str(num))