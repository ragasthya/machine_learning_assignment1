import pandas
import matplotlib
import matplotlib.pyplot as plt
from scipy import stats
import numpy as np
import sklearn.svm as svm
import sklearn.metrics as metrics
from tabulate import tabulate

# Question One: https://computing.dcu.ie/~humphrys/Notes/Neural/single.neural.html

def percentile(n):
    def percentile_(x):
        return np.percentile(x, n)
    percentile_.__name__ = 'percentile_%s' % n
    return percentile_

data = pandas.read_csv('WineQuality.csv', delimiter=',')

vars = ['fixed_acidity', 'volatile_acidity', 'citric_acid', 'residual_sugar', 'chlorides', 'free_sulfur_dioxide', 'total_sulfur_dioxide', 'density', 'pH', 'sulphates', 'alcohol']
# vars = ['fixed_acidity', 'volatile_acidity']

trainData = data[vars]

target_var = 'quality_grp'

yTrain = data[target_var]

length = len(data)

training = data[vars].values
test = data[target_var].values

# # RETAIN THIS CODE
# for var in vars:
#     data_groupings = {}
#     plt.figure()
#     bp = data.boxplot(column=var, by=target_var, vert=True)
#     plt.ylabel(var)
#     plt.show()

# RETAIN THIS CODE
data_hash = {}
for i in range(0, length):

    if(test[i] in data_hash.keys()):
        arr = data_hash[test[i]]
    else:
        arr = []
    arr.append(training[i].tolist())
    data_hash[test[i]] = arr
res = stats.ttest_ind(data_hash[0], data_hash[1], equal_var = False)
print(res)

def printData(df):
    variables = []
    for elem in df:
        variables.append(elem)

    dataPrint = [['_PredictedClass_', 0.0, 1.0]]

    for var in variables:
        dataSet = df[var]
        dataElem = [var, dataSet[0], dataSet[1]]
        dataPrint.append(dataElem)

    print(tabulate(dataPrint))


svm_Model = svm.LinearSVC(random_state = 20181111, max_iter = 10000)
thisFit = svm_Model.fit(trainData, yTrain)

print('Intercept:\n', thisFit.intercept_)
print('Weight Coefficients:\n', thisFit.coef_)
y_predictClass = thisFit.predict(trainData)

print('Mean Accuracy = ', metrics.accuracy_score(yTrain, y_predictClass))
trainData['_PredictedClass_'] = y_predictClass

svm_Mean = trainData.groupby('_PredictedClass_').mean()
printData(svm_Mean)

svm_25 = trainData.groupby('_PredictedClass_').agg(percentile(25))
printData(svm_25)

svm_75 = trainData.groupby('_PredictedClass_').agg(percentile(75))
printData(svm_75)