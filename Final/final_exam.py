import matplotlib.pyplot as plt
import numpy
import pandas
import random
import sklearn.ensemble as ensemble
import sklearn.metrics as metrics
import sklearn.model_selection as model_selection
import sklearn.tree as tree
import statsmodels.api as stats
import sklearn.neural_network as nn
import sklearn.naive_bayes as NB
from sklearn.neighbors import NearestNeighbors as kNN
from sklearn.neighbors import KNeighborsClassifier
import sklearn.linear_model as lm
import sklearn.svm as svm
import graphviz
import time

start_time = time.time()

training_data = pandas.read_csv('fleet_train.csv', delimiter=',')
test_data = pandas.read_csv('fleet_monitor_notscored_2.csv', delimiter=',')

target_variable = ['Maintenance_flag']
nominal_predictor_variable = ['Region']
other_variables = ['Accel_Pedal_Pos_D', 'Accel_Ssor_Total', 'Ambient_air_temp', 'CO2_in_g_per_km_Inst', 'Engine_Load', 'Engine_RPM', 'Litres_Per_100km_Inst', 'Mass_Air_Flow_Rate', 'Speed_GPS', 'Speed_OBD', 'Throttle_Pos_Manifold', 'Turbo_Boost_And_Vcm_Gauge', 'Vehicle_speed_sensor', 'Vibration', 'Voltage_Control_Module']

# omitted_variables = ['GPS_Bearing', 'GPS_Latitude', 'GPS_Longitude', 'Engine_Coolant_Temp', 'Engine_Oil_Temp', 'GPS_Altitude', 'Intake_Air_Temp', 'Intake_Manifold_Pressure', 'Trip_Distance', 'Trip_Time_journey']

def ModelMetrics (DepVar, EventValue, EventPredProb, Threshold):
    AUC = metrics.roc_auc_score(DepVar, EventPredProb)
    nObs = len(DepVar)
    RASE = 0
    MisClassRate = 0
    for i in range(nObs):
        p = EventPredProb[i]
        if (DepVar[i] == EventValue):
            RASE += (1.0 - p)**2
            if (p < Threshold):
                MisClassRate += 1
        else:
            RASE += p**2
            if (p >= Threshold):
                MisClassRate += 1
    RASE = numpy.sqrt(RASE / nObs)
    MisClassRate /= nObs
    return(AUC, RASE, MisClassRate)

def decision_tree_classifier(X_train, Y_train, X_test, Y_test, show=False):
    print("Decision Tree Classifier")
    classTree = tree.DecisionTreeClassifier(criterion='entropy', max_depth=2, random_state=60616)
    treeFit = classTree.fit(X_train, Y_train)
    treePredProb = classTree.predict_proba(X_test)

    test_vals = Y_test.values

    if(show):
        dot_data = tree.export_graphviz(treeFit, out_file=None)
        graph = graphviz.Source(dot_data)
        graph.render('decision_tree_output')

    results_dep = []
    results_prob = []

    for i in range(0, len(test_vals)):
        test = test_vals[i]
        prob = treePredProb[i]

        results_dep.append(test)
        if (test == 0):
            results_prob.append(prob[1])
        if (test == 1):
            results_prob.append(prob[0])

    # Accuracy_gbm = classTree.score(Y_test, treePredProb)

    return results_prob, results_dep, 0

def linear_SVC(X_train, Y_train, X_test, Y_test, show=False):
    svm_Model = svm.LinearSVC(verbose=1, random_state=20181205, max_iter=1000)
    thisFit = svm_Model.fit(X_train, Y_train)
    y_predictClass = thisFit.predict(X_test)

    results_dep = []
    results_prob = []

    test_vals = Y_test.values

    for i in range(0, len(test_vals)):
        test = test_vals[i]
        prob = y_predictClass[i]
        results_dep.append(test)
        results_prob.append(prob)

    if(show):
        print('Intercept:\n', thisFit.intercept_)
        print('Weight Coefficients:\n', thisFit.coef_)

    Accuracy_gbm = metrics.accuracy_score(Y_test, y_predictClass)

    return results_prob, results_dep, Accuracy_gbm

def gradient_boosting_classifier(X_train, Y_train, X_test, Y_test, show=False):
    print("Gradient Boosting Classifier")
    gbm = ensemble.GradientBoostingClassifier(loss='deviance', criterion='mse', n_estimators=2500, max_leaf_nodes=10)
    fit_gbm = gbm.fit(X_train, Y_train)
    predY_gbm = gbm.predict(X_test)

    results_dep = []
    results_prob = []

    test_vals = Y_test.values

    for i in range(0, len(test_vals)):
        test = test_vals[i]
        prob = predY_gbm[i]
        results_dep.append(test)
        results_prob.append(prob)

    if(show):
        Rsq_gbm = gbm.score(X_train, Y_train.ravel())
        print('R-Squared = ', Rsq_gbm)


    Accuracy_gbm = gbm.score(X_train, Y_train)

    return results_prob, results_dep, Accuracy_gbm

def neural_network(X_train, Y_train, X_test, Y_test, show=False):
    print("Neural Network")
    nHiddenNeuron = 20
    nLayer = 5
    nnObj = nn.MLPRegressor(hidden_layer_sizes=(nHiddenNeuron,) * nLayer, activation='tanh')
    fit_nn = nnObj.fit(X_train, Y_train.ravel())
    pred_nn = nnObj.predict(X_test)

    if(show):
        print('Output Activiation Function:', nnObj.out_activation_)
        print('R^2 of the Prediction:', nnObj.score(X_train, Y_train.ravel()))
        plt.figure(figsize=(30, 18))
        plt.plot(X_train.values, Y_train.values, linewidth=2, marker='+', color='black', label='Data')
        plt.plot(X_test.values, pred_nn, linewidth=2, marker='o', color='red', label='Prediction')
        plt.grid(True)
        plt.xlabel("x")
        plt.ylabel("y")
        plt.title("%d Hidden Layers, %d Hidden Neurons" % (nLayer, nHiddenNeuron))
        plt.show()

    results_dep = []
    results_prob = []
    test_vals = Y_test.values

    for i in range(0, len(test_vals)):
        test = test_vals[i]
        prob = pred_nn[i]
        results_dep.append(test)
        results_prob.append(prob)

    Accuracy_gbm = nnObj.score(X_train, Y_train)

    return results_prob, results_dep, Accuracy_gbm

def linear_regression(X_train, Y_train, X_test, Y_test, show=False):
    reg1 = lm.LinearRegression(fit_intercept=True, normalize=False, copy_X=True, n_jobs=None)
    fit1 = reg1.fit(X_train, Y_train)
    predY1 = reg1.predict(X_test)

    results_dep = []
    results_prob = []
    test_vals = Y_test.values

    for i in range(0, len(test_vals)):
        test = test_vals[i]
        prob = predY1[i]
        results_dep.append(test)
        results_prob.append(prob)

    if (show):
        RSq1 = reg1.score(X_train, Y_train)
        print('Model 1:', 'R-Squared = ', RSq1, '\n Intercept = ', reg1.intercept_, '\n Coefficients = ', reg1.coef_)

    Accuracy_gbm = reg1.score(X_train, Y_train)

    return results_prob, results_dep, Accuracy_gbm

models = [gradient_boosting_classifier, linear_SVC, neural_network]

train_region_1 = training_data.loc[training_data[nominal_predictor_variable[0]] == 1]
train_region_2 = training_data.loc[training_data[nominal_predictor_variable[0]] == 2]
train_region_3 = training_data.loc[training_data[nominal_predictor_variable[0]] == 3]

test_region_1 = test_data.loc[test_data[nominal_predictor_variable[0]] == 1]
test_region_2 = test_data.loc[test_data[nominal_predictor_variable[0]] == 2]
test_region_3 = test_data.loc[test_data[nominal_predictor_variable[0]] == 3]

train_regions = [train_region_1, train_region_2, train_region_3]
test_regions = [test_region_1, test_region_2, test_region_3]

total_regions = 3

final_results_dep = []
final_results_prob = []

mean_accuracy = 0

for index in range(total_regions):
    X_train = train_regions[index][other_variables]
    Y_train = train_regions[index][target_variable[0]]
    X_test = test_regions[index][other_variables]
    Y_test = test_regions[index][target_variable[0]]

    model = models[index]

    results_prob, results_dep, accuracy = model(X_train, Y_train, X_test, Y_test, show=True)

    final_results_dep = final_results_dep + results_dep
    final_results_prob = final_results_prob + results_prob
    mean_accuracy += accuracy

mean_accuracy /= 3

threshold = 0.20469083
AUC_test, RASE_test, MisClassRate_test = ModelMetrics(final_results_dep, 1, final_results_prob, threshold)

# accuracy = metrics.accuracy_score(final_results_dep, final_results_prob)
print('Area Under Curve: {:.7f}'.format(AUC_test))
print('Root Average Squared Error: {:.7f}'.format(RASE_test))
print('Misclassification Rate: {:.7f}'.format(MisClassRate_test))
print('Mean Accuracy: {:.7f}'.format(mean_accuracy))

print("--- %s seconds ---" % (time.time() - start_time))