import pandas
import itertools

def check(combs, list):
    for elem in combs:
        if elem[0] == list[0] or elem[1] == list[0] or elem[1] == list[0] or elem[1] == list[1]:
            return False
    return True

def combinations(arr):
    combs = []
    perms = list(itertools.permutations(arr))
    i = 1
    length = len(arr) - 1
    for perm in perms:
        list1 = list(perm)[:i]
        list2 = list(perm)[i:]
        if([list1, list2] not in combs and [list2, list1] not in combs) and check(combs, [list2, list1]):
            combs.append([list1, list2])
        if(i == length):
            i = 1
        else:
            i += 1

    return combs

def get_car_table(card_combinations, car_combinations, data):
    master_table = []
    for card_set in card_combinations:
        for car_set in car_combinations:
            print(str(card_set) + "\t" + str(car_set))
            table = get_frequency(card_set, car_set, data)
            table = sum_columns(table)
            master_table.append([car_set, card_set, table])
    return master_table

def sum_columns(table):
    for elem in table:
        sum = 0
        for i in range(0, len(elem)-1):
            sum += elem[i]
        elem[len(elem)-1] = sum
    return table

def get_frequency(card_set, car_set, data):
    table = []
    for cars in car_set:
        elem = []
        for cards in card_set:
            count = 0
            for row in data:
                if(row[0] in cars and row[2] in cards):
                    count += 1
            elem.append(count)
        elem.append(0)
        table.append(elem)
    return table

data = pandas.read_csv('CustomerSurveyData.csv', delimiter=',')
data_list = []


target_values1 = data['CarOwnership']
cars = {}
TOTAL = 0
for val in target_values1:
    TOTAL += 1
    if str(val) == 'nan':
        continue
    if val in cars.keys():
        value = cars[val]
        cars[val] = value+1
    else:
        cars[val] = 1

car_combinations = combinations(cars.keys())

target_values2 = data['JobCategory']
jobs = {}
for val in target_values2:
    if str(val) == 'nan':
        continue
    if val in jobs.keys():
        value = jobs[val]
        jobs[val] = value+1
    else:
        jobs[val] = 1

job_combinations = combinations(jobs.keys())

target_values3 = data['CreditCard']
cards = {}
for val in target_values3:
    if str(val) == 'nan':
        continue
    if val in cards.keys():
        value = cards[val]
        cards[val] = value+1
    else:
        cards[val] = 1

for i in range(0, TOTAL):
    val1 = target_values1[i]
    val2 = target_values2[i]
    val3 = target_values3[i]
    if(str(val1) == 'nan' or str(val2) == 'nan' or str(val3) == 'nan'):
        TOTAL -= 1
        continue
    else:
        data_list.append([val1, val2, val3])

card_combinations = cards.keys()

table = get_car_table(card_combinations, car_combinations, data_list)

for list in table:
    print(str(list))