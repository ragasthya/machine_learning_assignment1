import pandas

def factorial(n):
    fact = 1
    for i in range(1, n+1):
        fact = fact * i

def n_C_r(n, r):
    value = 1.0
    diff = n - r
    while True:
        if(n <= 1 and r <= 1 and diff <= 1):
            break
        value = value * n *(diff*r)
        if(n >= 1):
            n -= 1
        if(r >= 1):
            r -= 1
        if(diff >= 1):
            diff -= 1

    return value

data = pandas.read_csv('Purchase_Likelihood.csv', delimiter=',')

intercept_terms = data['A']

frequencies = {}
total = 0
for elem in intercept_terms:
    if elem not in frequencies.keys():
        frequencies[elem] = 1
    else:
        val = frequencies[elem]
        frequencies[elem] = val + 1
    total += 1

probabilities = {}
for key in frequencies.keys():
    probabilities[key] = frequencies[key]/total
    val = n_C_r(total, key)

likelihood_functions = {}
for key in frequencies.keys():
    combination_value = n_C_r(total, key)
    prob = probabilities[key]**key
    prob_other = (1 - probabilities[key])**(total - key)

    likelihood_functions[key] = prob

print(str(frequencies))
print(str(probabilities))
print(str(likelihood_functions))
print(str(total))