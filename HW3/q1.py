import pandas
import itertools

def total_car_gini(cars, cards, data):
    table = []
    for car in cars:
        elems = []
        total = 0
        for card in cards:
            freq = get_car_frequency([car], card, data)
            total += freq
            elems.append(freq)
        elems.append(total)
        table.append(elems)
    sums = [sum(x) for x in zip(*table)]
    total_sum = sums[len(sums)-1 : len(sums)][0]
    row_sum = 0
    for elem in sums[:len(sums)-1]:
        row_sum += (elem/total_sum)**2

    return (1-row_sum)

def total_job_gini(jobs, cards, data):
    table = []
    for job in jobs:
        elems = []
        total = 0
        for card in cards:
            freq = get_job_frequency([job], card, data)
            total += freq
            elems.append(freq)
        elems.append(total)
        table.append(elems)
    sums = [sum(x) for x in zip(*table)]
    total_sum = sums[len(sums) - 1: len(sums)][0]
    row_sum = 0
    for elem in sums[:len(sums) - 1]:
        row_sum += (elem / total_sum) ** 2

    return (1 - row_sum)

def check(combs, list):
    for elem in combs:
        if elem[0] == list[0] or elem[1] == list[0] or elem[1] == list[0] or elem[1] == list[1]:
            return False
    return True


def combinations(arr):
    combs = []
    perms = list(itertools.permutations(arr))
    i = 1
    length = len(arr) - 1
    for perm in perms:
        list1 = list(perm)[:i]
        list2 = list(perm)[i:]
        if ([list1, list2] not in combs and [list2, list1] not in combs) and check(combs, [list2, list1]):
            combs.append([list1, list2])
        if (i == length):
            i = 1
        else:
            i += 1

    return combs

def get_car_frequency(cars, card, data):
    count = 0
    for row in data:
        if(row[0] in cars and row[2] == card):
            count += 1
    return count

def get_car_table(cards, car_combinations, data):
    master_table = []
    for car_set in car_combinations:
        table = []
        for cars in car_set:
            elem = []
            total = 0
            for card in cards.keys():
                freq = get_car_frequency(cars, card, data)
                elem.append(freq)
                total += freq
            elem.append(total)
            table.append(elem)
        sums = [sum(x) for x in zip(*table)]
        table.append(sums)
        master_table.append([car_set, table])
    return master_table

def get_job_frequency(jobs, card, data):
    count = 0
    for row in data:
        if(row[1] in jobs and row[2] == card):
            count += 1
    return count

def check_master_table(master_table, job_set):
    for row in master_table:
        if(sorted(row[0][0]) == sorted(job_set[0]) or sorted(row[0][1]) == sorted(job_set[0])):
            return False
        if (sorted(row[0][0]) == sorted(job_set[1]) or sorted(row[0][1]) == sorted(job_set[1])):
            return False
    return True

def get_job_table(cards, job_combinations, data):
    master_table = []
    for job_set in job_combinations:
        table = []
        for jobs in job_set:
            elem = []
            total = 0
            for card in cards.keys():
                freq = get_job_frequency(jobs, card, data)
                elem.append(freq)
                total += freq
            elem.append(total)
            table.append(elem)
        sums = [sum(x) for x in zip(*table)]
        table.append(sums)
        if(check_master_table(master_table, job_set)):
            master_table.append([job_set, table])
    return master_table

def find_gini_indices(master_table, gini):
    for master_row in master_table:
        table = master_row[1]
        gini_indices = []
        all_total_row = table[2]
        all_total = all_total_row[len(all_total_row)-1 : len(all_total_row)][0]
        split_gini = 0
        for row in table[:len(table)-1]:
            total = row[len(row)-1 : len(row)][0]
            total_prob = total/all_total
            split_gini += (total_prob**2)
            sum = 0
            for elem in row[:len(row)-1]:
                prob = float(elem)/total
                sum += (prob**2)
            gini_indices.append(1 - sum)

        gini_indices.append(1 - split_gini)
        master_row.append(gini_indices)

        reduction = gini - (1 - split_gini)
        master_row.append(reduction)

    return master_table

def maximum_reduction(master_table):
    max = 0
    max_row = master_table[0]
    for row in master_table:
        if(row[3] > max):
            max = row[3]
            max_row = row
    return max_row

def print_table(table):
    i = 1
    for row in table:
        print(str(i) + ".\t" + str(row[0]) + "\t" + str(row[2]) + "\t" + str(row[3]))
        i += 1

data = pandas.read_csv('CustomerSurveyData.csv', delimiter=',')
data_list = []
target_values1 = data['CarOwnership']
cars = {}
TOTAL = 0
for val in target_values1:
    TOTAL += 1
    if str(val) == 'nan':
        continue
    if val in cars.keys():
        value = cars[val]
        cars[val] = value + 1
    else:
        cars[val] = 1

target_values2 = data['JobCategory']
jobs = {}
for val in target_values2:
    if str(val) == 'nan':
        continue
    if val in jobs.keys():
        value = jobs[val]
        jobs[val] = value + 1
    else:
        jobs[val] = 1

target_values3 = data['CreditCard']
cards = {}
for val in target_values3:
    if str(val) == 'nan':
        continue
    if val in cards.keys():
        value = cards[val]
        cards[val] = value + 1
    else:
        cards[val] = 1

for i in range(0, TOTAL):
    val1 = target_values1[i]
    val2 = target_values2[i]
    val3 = target_values3[i]
    if (str(val1) == 'nan' or str(val2) == 'nan' or str(val3) == 'nan'):
        TOTAL -= 1
        continue
    else:
        data_list.append([val1, val2, val3])

# print(str(data_list))

car_combinations = combinations(cars.keys())
job_combinations = combinations(jobs.keys())

car_gini = total_car_gini(cars.keys(), cards.keys(), data_list)
job_gini = total_job_gini(jobs.keys(), cards.keys(), data_list)

print("CAR GINI: " + str(car_gini))
car_table = get_car_table(cards, car_combinations, data_list)
car_table = find_gini_indices(car_table, car_gini)
print_table(car_table)

row = maximum_reduction(car_table)
print("MOST OPTIMAL: " + str(row[0]) + "\t" + str(row[2]) + "\t" + str(row[3]))

print("JOB GINI: " + str(job_gini))
job_table = get_job_table(cards, job_combinations, data_list)
job_table = find_gini_indices(job_table, job_gini)
print_table(job_table)

row = maximum_reduction(job_table)
print("MOST OPTIMAL: " + str(row[0]) + "\t" + str(row[2]) + "\t" + str(row[3]))