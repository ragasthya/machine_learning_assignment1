import numpy
import pandas
import sklearn.naive_bayes as NB

nominal_predictors = ['group_size', 'homeowner', 'married_couple']
target_variable = ['A']
data = pandas.read_csv('Purchase_Likelihood.csv', delimiter=',')

X = data[nominal_predictors]
y = data[target_variable]

classifier = NB.MultinomialNB().fit(X, y)
total = float(len(X))

class_prob = []
for elem in classifier.class_count_:
    class_prob.append(elem/total)

print('Class Count:\n', classifier.class_count_)
print('Log Class Probability:\n', classifier.class_log_prior_ )
print('Feature Count (after adding alpha):\n', classifier.feature_count_)
print('Log Feature Probability:\n', classifier.feature_log_prob_)
print('Class Probability:\n', class_prob)

predProb = classifier.predict_proba(X)
print('Predicted Conditional Probability (Training):', predProb)

max_indices = []
max = predProb[0][1]
for i in range(0, len(predProb)):
    if(predProb[i][1] > max):
        max_indices = []
        max_indices.append(i)
        max = predProb[i][1]
    elif (predProb[i][1] == max):
        max_indices.append(i)
        max = predProb[i][1]

print(len(max_indices))

X_test = numpy.array([[1,0,0],
                      [2,1,1],
                      [3,1,1],
                      [4,0,0]])

print('Predicted Conditional Probability (Testing):\n', classifier.predict_proba(X_test))

print('Values where Prob(A=1) is maximum: ')

print('GROUP \t HOME \t MARRIED \t P(A=0) \t P(A=1) \t P(A=1)')
predictors = []
for max_index in max_indices:
    grp = X['group_size'][max_index]
    home = X['homeowner'][max_index]
    married = X['married_couple'][max_index]

    if(not [grp, home, married] in predictors):
        predictors.append([grp, home, married])

for elem in predictors:
    grp = elem[0]
    home = elem[1]
    married = elem[2]

    X_test = numpy.array([[grp,home,married]])

    # print('Predicted Conditional Probability (Testing):\n', classifier.predict_proba(X_test))
    probs = classifier.predict_proba(X_test)
    print('{:.0f} \t {:.0f} \t {:.0f} \t '.format(grp, home, married))
    print(str(probs) + "\n")