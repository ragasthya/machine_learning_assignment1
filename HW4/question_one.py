import matplotlib.pyplot as plt
import numpy
import pandas
import sklearn.cluster as cluster
import sklearn.decomposition as decomposition
import sklearn.metrics as metrics
import sklearn.linear_model as linear_model

usecols = []
years = []
for i in range(0, 12):
    usecols.append('Crude Rate ' + str(2000 + i))
    years.append(str(2000 + i))

data = pandas.read_csv('ChicagoDiabetes.csv', delimiter=',')

X = data[usecols]

nObs = X.shape[0]
nVar = X.shape[1]

pandas.plotting.scatter_matrix(X, figsize=(20,20), c = 'red', diagonal='hist', hist_kwds={'color':['burlywood']})

XCorrelation = X.corr(method = 'pearson', min_periods = 1)

# Extract the Principal Components
_thisPCA = decomposition.PCA(n_components = nVar)
_thisPCA.fit(X)

cumsum_variance_ratio = numpy.cumsum(_thisPCA.explained_variance_ratio_)
cumsum_variance = numpy.cumsum(_thisPCA.explained_variance_)

print('Descriptive Statistics: \n', X.describe())

print('Number of Observations: ', str(nObs))
print('Number of Variables: ', str(nVar))

print('Explained Variance: \n', _thisPCA.explained_variance_)
print('Explained Variance Ratio: \n', _thisPCA.explained_variance_ratio_)
print('Cumulative Explained Variance: \n', cumsum_variance)
print('Cumulative Explained Variance Ratio: \n', cumsum_variance_ratio)
print('Principal Components: \n', _thisPCA.components_)

plt.plot(_thisPCA.explained_variance_ratio_, marker = 'o')
plt.xlabel('Index')
plt.ylabel('Explained Variance Ratio')
plt.xticks(numpy.arange(0,nVar))
plt.axhline((1/nVar), color = 'r', linestyle = '--')
plt.grid(True)
plt.show()

indices = list(range(0, len(cumsum_variance)))
zeros = []
reciprocal_variances = []
print("Reciprocal variances: ")
for elem in _thisPCA.explained_variance_:
    reciprocal_variances.append(1 - float(elem))
    zeros.append(0)

plt.plot(indices, reciprocal_variances, linewidth=2, marker='o', c='black')
plt.plot(indices, _thisPCA.explained_variance_, linewidth=2, marker='o', c='red')
plt.plot(indices, zeros, linewidth=2, c='green')
plt.grid(True)
plt.axis(aspect='equal')
plt.xlabel('Index')
plt.ylabel('Explained Variances and Reciprocal')
plt.axis(aspect='equal')
plt.legend(title = 'Explained Variance and Reciprocal', fontsize = 12, markerscale = 2)
plt.show()

plt.plot(indices, cumsum_variance, linewidth=2, marker='o')
plt.grid(True)
plt.axis(aspect='equal')
plt.xlabel('Index')
plt.ylabel('Culumlative Sum Explained Variance')
plt.axis(aspect='equal')
plt.legend(title = 'Explained Variance Cum.', fontsize = 12, markerscale = 2)
plt.show()

first2PC = _thisPCA.components_[:, [0,1]]
print('Principal Component: \n', first2PC)

print('The first two principal components account for {:.4f}% of the total variance.'.format(cumsum_variance_ratio[1]*100))

_thisPCA = decomposition.PCA(n_components = 2)
X_transformed = pandas.DataFrame(_thisPCA.fit_transform(X))

maxNClusters = 16
minNClusters = 2

nClusters = numpy.zeros(maxNClusters - 1)
Elbow = numpy.zeros(maxNClusters - 1)
Silhouette = numpy.zeros(maxNClusters - 1)
TotalWCSS = numpy.zeros(maxNClusters - 1)
Inertia = numpy.zeros(maxNClusters - 1)

for c in range(minNClusters, maxNClusters - 1):
    KClusters = c + 2
    nClusters[c] = KClusters

    kmeans = cluster.KMeans(n_clusters=KClusters, random_state=20181010).fit(X_transformed)

    # The Inertia value is the within cluster sum of squares deviation from the centroid
    Inertia[c] = kmeans.inertia_

    if (KClusters > 1):
        Silhouette[c] = metrics.silhouette_score(X_transformed, kmeans.labels_)
    else:
        Silhouette[c] = float('nan')

    WCSS = numpy.zeros(KClusters)
    nC = numpy.zeros(KClusters)

    for i in range(nObs):
        k = kmeans.labels_[i]
        nC[k] += 1
        diff = X_transformed.iloc[i,] - kmeans.cluster_centers_[k]
        WCSS[k] += diff.dot(diff)

    Elbow[c] = 0
    for k in range(KClusters):
        Elbow[c] += (WCSS[k] / nC[k])
        TotalWCSS[c] += WCSS[k]

    print("The", KClusters, "Cluster Solution Done")

print("N Clusters\t Inertia\t Total WCSS\t Elbow Value\t Silhouette Value:")
for c in range(maxNClusters - 1):
    print('{:.0f} \t {:.4f} \t {:.4f} \t {:.4f} \t {:.4f}'
          .format(nClusters[c], Inertia[c], TotalWCSS[c], Elbow[c], Silhouette[c]))

# Draw the Elbow and the Silhouette charts
plt.plot(nClusters, Elbow, linewidth=2, marker='o')
plt.grid(True)
plt.xlabel("Number of Clusters")
plt.ylabel("Elbow Value")
plt.xticks(numpy.arange(2, maxNClusters, 1))
plt.show()

plt.plot(nClusters, Silhouette, linewidth=2, marker='o')
plt.grid(True)
plt.xlabel("Number of Clusters")
plt.ylabel("Silhouette Value")
plt.xticks(numpy.arange(2, maxNClusters, 1))
plt.show()

kmeans = cluster.KMeans(n_clusters=4, random_state=20181010).fit(X_transformed)
X_transformed['Cluster ID'] = kmeans.labels_
X['Cluster ID'] = kmeans.labels_

carray = ['red', 'orange', 'green', 'black', 'blue']
plt.figure(figsize=(10,10))
for i in range(4):
    subData = X_transformed[X_transformed['Cluster ID'] == i]
    plt.scatter(x = subData[0], y = subData[1], c = carray[i], label = i, s = 25)
plt.grid(True)
plt.axis(aspect = 'equal')
plt.xlabel('PC1')
plt.ylabel('PC2')
plt.axis(aspect = 'equal')
plt.legend(title = 'Cluster ID', fontsize = 12, markerscale = 2)
plt.show()

print("Index", end="")
for col in years:
    print("\t", end="")
    print(str(col), end="")
print("\tCluster ID")
for i in range(0, len(X)):
    print(str(i), end="")
    for col in usecols:
        print("\t", end="")
        print(str(X[col][i]), end="")
    print("\t", end="")
    print(X['Cluster ID'][i])

print('Transformed X: \n', X_transformed)

annual_rates = [25.4, 25.8, 27.2, 25.4, 26.2, 26.6, 27.4, 28.7, 27.9, 27.5, 26.8, 25.6]

for i in range(0, 4):
    cluster_count = 0
    for index in range(0, len(X)):
        rates = []
        if(X['Cluster ID'][index] == i):
            cluster_count += 1
            for col in usecols:
                rates.append(X[col][index])
            plt.scatter(x = years, y = rates, c = carray[i], label = i, s = 25)

    print("Cluster: " + str(i) + "\tCount: " + str(cluster_count))
plt.scatter(x = years, y = annual_rates, c = 'yellow', label = i, s = 25)
plt.grid(True)
plt.axis(aspect='equal')
plt.xlabel('Year')
plt.ylabel('Rates')
plt.axis(aspect='equal')
plt.show()