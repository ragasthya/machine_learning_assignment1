import pandas
import math
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn import metrics
import matplotlib.pyplot as plt
from sklearn.metrics import roc_auc_score
import numpy as np
import statsmodels.api as stats
from sklearn import tree
import statsmodels.api as api

target_variable = 'CLAIM_FLAG'
nominal_predictor_variable = 'CREDIT_SCORE_BAND'
predictor_variables = ['BLUEBOOK_1000', 'CUST_LOYALTY', 'MVR_PTS', 'TIF', 'TRAVTIME']

NEIGHBOR_NUMBER = 3
MAX_DEPTH = 10
RANDOM_SEED = 20181010
MAX_ITERATIONS = 100

entiries_file = pandas.read_csv('policy_2001.csv', delimiter=',')
MAX_ITEMS = len(entiries_file)

def claim_rated(file):
    total_items = len(file)
    count = 0
    for item in file[target_variable]:
        if(item == 1):
            count += 1
    return float(count)/total_items

def misclassification_rate(X, y):
    X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=RANDOM_SEED)
    logreg = LogisticRegression()
    logreg.fit(X_train, y_train)
    y_pred_class = logreg.predict(X_test)
    accurary = metrics.accuracy_score(y_test, y_pred_class)
    return 1 - accurary

def area_under_curve(class_prob):
    events = np.delete(class_prob, 0, 1)
    n_events = np.delete(class_prob, 1, 1)

    two_way = []
    concordant = 0
    discordant = 0
    tied = 0
    for event in events:
        arr = []
        for n_event in n_events:
            arr.append([event, n_event])
            if(event > n_event):
                concordant += 1
            elif(event < n_event):
                discordant += 1
            else:
                tied += 1
        two_way.append(arr)

    AUC = 0.5+0.5*(concordant-discordant)/(concordant+discordant+tied)
    return AUC

def root_mean_square(class_prob):
    events = np.delete(class_prob, 0, 1)
    n_events = np.delete(class_prob, 1, 1)

    no_events = len(events)
    no_n_events = len(n_events)

    sum_events = 0
    for event in events:
        sum_events += (1 - event)**2

    sum_n_events = 0
    for n_event in n_events:
        sum_n_events += (0 - n_event) ** 2

    ase = (sum_events + sum_n_events) / (no_events + no_n_events)
    return math.sqrt(ase)


def misclassifications(data, predicted):
    data = data.reset_index().values
    data = np.delete(data, 0, 1)
    N = len(data)
    misclassified = 0
    for i in range(N):
        if(not data[i][0] == predicted[i]):
            misclassified += 1

    return misclassified/N


def ROC_Coordinates(data, class_result, class_prob, color):
    Y = class_result
    N = len(Y)
    scores = []
    for entry in class_prob:
        scores.append(entry[1])
    scores = np.unique(np.array(scores))

    total_events = 0
    total_nevents = 0
    for val in class_result:
        if(val == 1):
            total_events += 1
        else:
            total_nevents += 1

    sensitivity = []
    one_minus_specificity = []
    for score in scores:
        i = 0
        tp = 0
        tn = 0
        for entry in class_result:
            prob = class_prob[i][1]
            if(entry == 1 and prob >= score):
                tp += 1
            elif(entry == 0 and prob < score):
                tn += 1
            i += 1
        sensitivity.append(float(tp)/total_events)
        one_minus_specificity.append(1 - (float(tn)/total_nevents))

    rocData = pandas.DataFrame({'CutOff': scores, 'Sensitivity': sensitivity, 'OneMinusSpecificity': one_minus_specificity})

    plt.plot(rocData['OneMinusSpecificity'], rocData['Sensitivity'], marker='o', color=color, linestyle='solid', linewidth=2, markersize=6)

def nearest_neighbors(data):
    kNNSpec = KNeighborsClassifier(n_neighbors=3, algorithm='brute', metric='euclidean')
    nbrs = kNNSpec.fit(data[predictor_variables], data[target_variable])
    class_result = nbrs.predict(data[predictor_variables])
    roc_accuracy = roc_auc_score(data[target_variable], class_result)
    class_prob = nbrs.predict_proba(data[predictor_variables])
    ROC_Coordinates(data, class_result, class_prob, 'red')
    print("ROC Nearest Neighbors: " + str(roc_accuracy))
    auc = area_under_curve(class_prob)
    rms = root_mean_square(class_prob)
    missed = misclassifications(data[target_variable], class_result)
    print("AUC Nearest Neighbors: " + str(auc))
    print("RMS Nearest Neighbors: " + str(rms))
    print("Misclassification Rate Nearest Neighbors: " + str(missed))

def classification_tree(data):
    classTree = tree.DecisionTreeClassifier(criterion='entropy', max_depth=MAX_DEPTH, random_state=RANDOM_SEED)
    D = np.reshape(np.asarray(data[predictor_variables]), (len(data), len(predictor_variables)))
    hmeq_DT = classTree.fit(D, data[target_variable])
    class_result = classTree.predict(data[predictor_variables])
    class_prob = classTree.predict_proba(data[predictor_variables])
    roc_accuracy = roc_auc_score(data[target_variable], class_result)
    ROC_Coordinates(data, class_result, class_prob, 'blue')
    print("ROC Classification Tree: " + str(roc_accuracy))
    auc = area_under_curve(class_prob)
    rms = root_mean_square(class_prob)
    missed = misclassifications(data[target_variable], class_result)
    print("AUC Classification Tree: " + str(auc))
    print("RMS Classification Tree: " + str(rms))
    print("Misclassification Rate Classification Tree: " + str(missed))

def logistic_model(data):
    y = data[target_variable].astype('category')
    X = data[predictor_variables]

    logit = api.MNLogit(y, X)

    thisFit = logit.fit(method='newton', full_output=True, maxiter=MAX_ITERATIONS, tol=1e-8)
    thisParameter = thisFit.params
    y_predProb = thisFit.predict(X)
    y_predict = pandas.to_numeric(y_predProb.idxmax(axis=1))
    y_predictClass = y.cat.categories[y_predict]
    # print(str(y_predictClass))
    roc_accuracy = roc_auc_score(data[target_variable], y_predictClass)
    class_prob = y_predProb.reset_index().values
    class_prob = np.delete(class_prob, 0, 1)
    ROC_Coordinates(data, y_predictClass, class_prob, 'green')
    print("ROC Classification Tree: " + str(roc_accuracy))
    auc = area_under_curve(class_prob)
    rms = root_mean_square(class_prob)
    missed = misclassifications(data[target_variable], y_predictClass)
    print("AUC Classification Tree: " + str(auc))
    print("RMS Classification Tree: " + str(rms))
    print("Misclassification Rate Classification Tree: " + str(missed))

target = entiries_file[target_variable]
nominal_predictors = entiries_file[nominal_predictor_variable]
predictors = entiries_file[predictor_variables]

TRAINING_LEN = math.ceil(0.7 * MAX_ITEMS) - 1

print(str(TRAINING_LEN))
print(str(MAX_ITEMS - TRAINING_LEN))

training, test = train_test_split(entiries_file, test_size=0.3, random_state=RANDOM_SEED)

print(str(len(training)))
print(str(len(test)))
print(str(len(training) + len(test)))

print("Claim rate for Training: " + str(claim_rated(training)))
print("Claim rate for Test: " + str(claim_rated(test)))

misclassification_rate = misclassification_rate(test[predictor_variables], test[target_variable])
print(str(misclassification_rate))

ax = plt.gca()
ax.set_aspect('equal')
plt.figure(figsize=(6, 6))

nearest_neighbors(test)

classification_tree(test)

logistic_model(test)

plt.plot([0, 1], [0, 1], color='black', linestyle=':')
plt.grid(True)
plt.xlabel("1 - Specificity")
plt.ylabel("Sensitivity")
plt.show()

# print(str(roc_auc_score(test[target_variable], test[predictor_variables])))