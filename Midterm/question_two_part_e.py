import pandas
import math
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn import metrics
import matplotlib.pyplot as plt
from sklearn.metrics import roc_auc_score
import numpy as np
import statsmodels.api as stats
from sklearn import tree
import statsmodels.api as api

target_variable = 'CLAIM_FLAG'
nominal_predictor_variable = 'CREDIT_SCORE_BAND'
predictor_variables = ['BLUEBOOK_1000', 'CUST_LOYALTY', 'MVR_PTS', 'TIF', 'TRAVTIME']

NEIGHBOR_NUMBER = 3
MAX_DEPTH = 10
RANDOM_SEED = 20181010
MAX_ITERATIONS = 100

entiries_file = pandas.read_csv('policy_2001.csv', delimiter=',')
MAX_ITEMS = len(entiries_file)


def compute_lift_coordinates(
        DepVar,  # The column that holds the dependent variable's values
        EventValue,  # Value of the dependent variable that indicates an event
        EventPredProb,  # The column that holds the predicted event probability
        Debug='N'):  # Show debugging information (Y/N)

    # Find out the number of observations
    nObs = len(DepVar)

    # Get the quantiles
    quantileCutOff = np.percentile(EventPredProb, np.arange(0, 100, 10))
    nQuantile = len(quantileCutOff)

    quantileIndex = np.zeros(nObs)
    for i in range(nObs):
        iQ = nQuantile
        EPP = EventPredProb.iloc[i]
        for j in range(1, nQuantile):
            if (EPP > quantileCutOff[-j]):
                iQ -= 1
        quantileIndex[i] = iQ

    # Construct the Lift chart table
    countTable = pandas.crosstab(quantileIndex, DepVar)
    decileN = countTable.sum(1)
    decilePct = 100 * (decileN / nObs)
    gainN = countTable[EventValue]
    totalNResponse = gainN.sum(0)
    gainPct = 100 * (gainN / totalNResponse)
    responsePct = 100 * (gainN / decileN)
    overallResponsePct = 100 * (totalNResponse / nObs)
    lift = responsePct / overallResponsePct

    LiftCoordinates = pandas.concat([decileN, decilePct, gainN, gainPct, responsePct, lift],
                                    axis=1, ignore_index=True)
    LiftCoordinates = LiftCoordinates.rename({0: 'Decile N',
                                              1: 'Decile %',
                                              2: 'Gain N',
                                              3: 'Gain %',
                                              4: 'Response %',
                                              5: 'Lift'}, axis='columns')

    # Construct the Accumulative Lift chart table
    accCountTable = countTable.cumsum(axis=0)
    decileN = accCountTable.sum(1)
    decilePct = 100 * (decileN / nObs)
    gainN = accCountTable[EventValue]
    gainPct = 100 * (gainN / totalNResponse)
    responsePct = 100 * (gainN / decileN)
    lift = responsePct / overallResponsePct

    accLiftCoordinates = pandas.concat([decileN, decilePct, gainN, gainPct, responsePct, lift],
                                       axis=1, ignore_index=True)
    accLiftCoordinates = accLiftCoordinates.rename({0: 'Acc. Decile N',
                                                    1: 'Acc. Decile %',
                                                    2: 'Acc. Gain N',
                                                    3: 'Acc. Gain %',
                                                    4: 'Acc. Response %',
                                                    5: 'Acc. Lift'}, axis='columns')

    if (Debug == 'Y'):
        print('Number of Quantiles = ', nQuantile)
        print(quantileCutOff)
        _u_, _c_ = np.unique(quantileIndex, return_counts=True)
        print('Quantile Index: \n', _u_)
        print('N Observations per Quantile Index: \n', _c_)
        print('Count Table: \n', countTable)
        print('Accumulated Count Table: \n', accCountTable)

    return (LiftCoordinates, accLiftCoordinates)

def claim_rated(file):
    total_items = len(file)
    count = 0
    for item in file[target_variable]:
        if(item == 1):
            count += 1
    return float(count)/total_items

def misclassification_rate(X, y):
    X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=RANDOM_SEED)
    logreg = LogisticRegression()
    logreg.fit(X_train, y_train)
    y_pred_class = logreg.predict(X_test)
    accurary = metrics.accuracy_score(y_test, y_pred_class)
    return 1 - accurary

def lift_curve(data, predict_probs, color):
    Y = data[target_variable].astype('category')
    y_train = pandas.DataFrame({target_variable: Y.values})
    # print(type(y_train))
    # print(y_train)
    score_train = pandas.concat([y_train, predict_probs], axis=1)
    print(score_train)
    lift_coordinates, acc_lift_coordinates = compute_lift_coordinates(DepVar=score_train[target_variable], EventValue=1, EventPredProb=score_train["1"], Debug='Y')
    plt.plot(acc_lift_coordinates.index, acc_lift_coordinates['Acc. Lift'], marker='o', color=color, linestyle='solid', linewidth=2, markersize=6)

def nearest_neighbors(data):
    kNNSpec = KNeighborsClassifier(n_neighbors=3, algorithm='brute', metric='euclidean')
    nbrs = kNNSpec.fit(data[predictor_variables], data[target_variable])
    class_result = nbrs.predict(data[predictor_variables])
    roc_accuracy = roc_auc_score(data[target_variable], class_result)
    class_prob = nbrs.predict_proba(data[predictor_variables])

    prob_df = pandas.DataFrame({'0': class_prob[:, 0], '1': class_prob[:, 1]})
    # print(prob_df)
    lift_curve(data, prob_df, 'blue')

    print("ROC Nearest Neighbors: " + str(roc_accuracy))

def classification_tree(data):
    classTree = tree.DecisionTreeClassifier(criterion='entropy', max_depth=MAX_DEPTH, random_state=RANDOM_SEED)
    D = np.reshape(np.asarray(data[predictor_variables]), (len(data), len(predictor_variables)))
    hmeq_DT = classTree.fit(D, data[target_variable])
    class_result = classTree.predict(data[predictor_variables])
    class_prob = classTree.predict_proba(data[predictor_variables])
    roc_accuracy = roc_auc_score(data[target_variable], class_result)
    prob_df = pandas.DataFrame({'0': class_prob[:, 0], '1': class_prob[:, 1]})
    # print(prob_df)
    lift_curve(data, prob_df, 'black')
    print("ROC Classification Tree: " + str(roc_accuracy))

def logistic_model(data):
    y = data[target_variable].astype('category')
    X = data[predictor_variables]

    logit = api.MNLogit(y, X)

    thisFit = logit.fit(method='newton', full_output=True, maxiter=100, tol=1e-8)
    thisParameter = thisFit.params
    y_predProb = thisFit.predict(X)
    y_predict = pandas.to_numeric(y_predProb.idxmax(axis=1))
    y_predictClass = y.cat.categories[y_predict]
    roc_accuracy = roc_auc_score(data[target_variable], y_predictClass)
    class_prob = y_predProb.reset_index().values
    class_prob = np.delete(class_prob, 0, 1)
    prob_df = pandas.DataFrame({'0': class_prob[:, 0], '1': class_prob[:, 1]})
    # print(prob_df)
    lift_curve(data, prob_df, 'red')
    print("ROC Logistic Model: " + str(roc_accuracy))

target = entiries_file[target_variable]
nominal_predictors = entiries_file[nominal_predictor_variable]
predictors = entiries_file[predictor_variables]

TRAINING_LEN = math.ceil(0.7 * MAX_ITEMS) - 1

print(str(TRAINING_LEN))
print(str(MAX_ITEMS - TRAINING_LEN))

training, test = train_test_split(entiries_file, test_size=0.3, random_state=RANDOM_SEED)

print(str(len(training)))
print(str(len(test)))
print(str(len(training) + len(test)))

print("Claim rate for Training: " + str(claim_rated(training)))
print("Claim rate for Test: " + str(claim_rated(test)))

misclassification_rate = misclassification_rate(test[predictor_variables], test[target_variable])
print(str(misclassification_rate))

ax = plt.gca()
ax.set_aspect('equal')
plt.figure(figsize=(6, 6))

nearest_neighbors(test)

classification_tree(test)

logistic_model(test)

plt.title("Testing Partition")
plt.grid(True)
plt.xticks(np.arange(1,11, 1))
plt.xlabel("Decile Group")
plt.ylabel("Accumulated Lift")
plt.show()

# print(str(roc_auc_score(test[target_variable], test[predictor_variables])))