import math
import pandas
import sklearn
import numpy as np
from numpy import percentile
from sklearn import cluster
from sklearn import tree
import statistics

import matplotlib.pyplot as plt
from sklearn.cluster import KMeans

MAX_CLUSTERS = 15
MIN_CLUSTERS = 2
RANDOM_SEED = 20181010

class Entry:
    def __init__(self, case, ward, month_created, n_potholes_filled, n_days, latitude, longitude):
        self.case = case                                        # CASE_SEQUENCE
        self.ward = ward                                        # WARD
        self.month_created = month_created                      # CREATION_MONTH
        self.n_potholes_filled = n_potholes_filled              # N_POTHOLES_FILLED_ON_BLOCK
        self.n_days = n_days                                    # N_DAYS_FOR_COMPLETION
        self.latitude = latitude                                # LATITUDE
        self.longitude = longitude                              # LONGITUDE
        self.log_n_potholes_filled = math.log(float(n_potholes_filled))
        self.log_n_days = math.log(float(1 + n_days))
        self.cluster_id = 0

    def to_string(self):
        return str(self.case) + ". " + str(self.ward) + "\t" + str(self.month_created) + "\t" + str(self.n_potholes_filled) + "\t" + str(self.n_days) + "\t" + str(self.latitude) + "\t" + str(self.longitude) + "\t" + str(self.log_n_potholes_filled) + "\t" + str(self.log_n_days)

def update_cluster(id, case, entries):
    for entry in entries:
        if(entry.case == case):
            entry.cluster_id = id

def kmeans_clusters(entries):
    X1, X2, X3, X4 = [], [], [], []
    for entry in entries:
        X1.append(entry.log_n_potholes_filled)
        X2.append(entry.log_n_days)
        X3.append(entry.latitude)
        X4.append(entry.longitude)

    x1, x2, x3, x4 = np.array(X1), np.array(X2), np.array(X3), np.array(X4)
    plt.plot()
    X = np.array(list(zip(x1, x2, x3, x4))).reshape(len(entries), 4)
    colors = ['b', 'g', 'r']
    markers = ['o', 'v', 's']

    # ELBOW
    ssd = []
    K = range(MIN_CLUSTERS, MAX_CLUSTERS+1)
    for k in K:
        kmeanModel = KMeans(n_clusters=k).fit(X)
        kmeanModel.fit(X)
        ssd.append(kmeanModel.inertia_)
    print(str(len(ssd)))
    for k in K:
        print(str(k) + '\t' + str(ssd[k - 2]))

    plt.plot(K, ssd, 'bx-')
    plt.xlabel('k')
    plt.ylabel('Sum of Square Distances')
    plt.title('The Elbow Method showing the optimal k')
    plt.show()

    Optimal_NumberOf_Components = K[ssd.index(max(ssd))]
    print("Optimal number of components is: " + str(Optimal_NumberOf_Components))

    sum = Optimal_NumberOf_Components

    # SILHOUTTEE
    obs = X
    silhouette_score_values = list()

    NumberOfClusters = range(MIN_CLUSTERS, MAX_CLUSTERS+1)

    for i in NumberOfClusters:
        classifier = cluster.KMeans(i, init='k-means++', n_init=10, max_iter=300, tol=0.0001, verbose=0, random_state=RANDOM_SEED, copy_x=True)
        classifier.fit(obs)
        labels = classifier.predict(obs)
        sil_val = sklearn.metrics.silhouette_score(obs, labels, metric='euclidean', sample_size=len(entries), random_state=RANDOM_SEED)
        silhouette_score_values.append(sil_val)
        print(str(i) + '\t' + str(sil_val))

    plt.plot(NumberOfClusters, silhouette_score_values)
    plt.title("Silhouette score values vs Numbers of Clusters ")
    plt.show()

    Optimal_NumberOf_Components = NumberOfClusters[silhouette_score_values.index(max(silhouette_score_values))]
    print("Optimal number of components is: " + str(Optimal_NumberOf_Components))

    return sum, X

def group_by_id(clusters, data, entries):
    kmeans = KMeans(n_clusters=clusters, random_state=RANDOM_SEED).fit(X)
    ClusterResult = data
    ClusterResult['CLUSTER_ID'] = kmeans.labels_

    for i in range(clusters):
        ClusterResult.loc[ClusterResult['CLUSTER_ID'] == i]

    for i in range(len(data)):
        case = data['CASE_SEQUENCE'][i]
        cluster = data['CLUSTER_ID'][i]
        update_cluster(int(cluster), int(case), entries)

def box_plots(entiries_file, clusters):
    # N_POTHOLES_FILLED_ON_BLOCK, N_DAYS_FOR_COMPLETION, LATITUDE, LONGITUDE
    variables = ['N_POTHOLES_FILLED_ON_BLOCK', 'N_DAYS_FOR_COMPLETION', 'LATITUDE', 'LONGITUDE']
    for variable in variables:
        print(variable)
        for i in range(0, clusters):
            print("\t" + str(i))
            data = []
            for j in range(0, len(entiries_file)):
                if(entiries_file['CLUSTER_ID'][j] == i):
                    data.append(entiries_file[variable][j])
            data_min, data_max = min(data), max(data)
            quartiles = percentile(data, [25, 50, 75])
            print("\t\tMIN: " + str(data_min))
            print("\t\tMAX: " + str(data_max))
            print("\t\t25: " + str(quartiles[0]))
            print("\t\t50: " + str(quartiles[1]))
            print("\t\t75: " + str(quartiles[2]))
        print("")

def scatter_plot(entries_file):
    y = np.array(entiries_file['LATITUDE'])
    x = np.array(entiries_file['LONGITUDE'])

    colors = (0, 0, 0)
    area = np.pi * 1

    plt.scatter(x, y, s=area, c=colors, alpha=1)
    plt.title('Latitude vs Longitude')
    plt.xlabel('x')
    plt.ylabel('y')
    plt.show()

def decision_tree(entries_file):
    targetVar = 'CLUSTER_ID'

    predList = ['N_POTHOLES_FILLED_ON_BLOCK', 'N_DAYS_FOR_COMPLETION', 'LATITUDE', 'LONGITUDE']

    x_train = entries_file[predList]
    y_train = entries_file[targetVar]
    classTree = tree.DecisionTreeClassifier(criterion='entropy', max_depth=2)

    hmeq_dt = classTree.fit(x_train, y_train)
    print('Accuracy of Decision Tree classifier on training set: {:.2f}'.format(classTree.score(x_train, y_train)))

    predicts = classTree.predict([entiries_file[targetVar]])
    predicts_prob = classTree.predict_proba([entiries_file[targetVar]])
    # print(hmeq_dt)

    import graphviz
    dot_data = tree.export_graphviz(hmeq_dt, out_file=None)
    graph = graphviz.Source(dot_data)
    graph.render('decision_tree_output')

entries = []

entiries_file = pandas.read_csv('ChicagoCompletedPotHole.csv', delimiter=',')
N_ENTRIES = len(entiries_file)

for i in range(0, N_ENTRIES):
    entry = Entry(entiries_file['CASE_SEQUENCE'][i], entiries_file['WARD'][i], entiries_file['CREATION_MONTH'][i], entiries_file['N_POTHOLES_FILLED_ON_BLOCK'][i], entiries_file['N_DAYS_FOR_COMPLETION'][i], entiries_file['LATITUDE'][i], entiries_file['LONGITUDE'][i])
    entries.append(entry)

# print(str(N_ENTRIES))
clusters, X = kmeans_clusters(entries)
# print(str(clusters))

group_by_id(2, entiries_file, entries)
# box_plots(entiries_file, int(clusters))
box_plots(entiries_file, 2)

scatter_plot(entiries_file)
decision_tree(entiries_file)